# Visual Studio Code

## Some theory

In `developer mode` one would want to edit/build/debug this app.
This is currertly handled by some magic scripts and `.json` files.

### Settings

`settings.json`[1] is supposed to be the top level config, which should be modified accordingly.
It is used by `tasks.json`[2], `launch.json`[3] and passed on to `cross-compile.sh`[4] and `deploy.sh`[5].

[1] https://gitlab.com/exempli-gratia/mqtt-key-event-daemon/-/blob/master/.vscode/settings.json

[2] https://gitlab.com/exempli-gratia/mqtt-key-event-daemon/-/blob/master/.vscode/tasks.json

[3] https://gitlab.com/exempli-gratia/mqtt-key-event-daemon/-/blob/master/.vscode/launch.json

[4] https://gitlab.com/exempli-gratia/mqtt-key-event-daemon/-/blob/master/cross-compile.sh

[5] https://gitlab.com/exempli-gratia/mqtt-key-event-daemon/-/blob/master/deploy.sh

### Tasks

`tasks.json`[2] defines tasks like `cross-compile` and `deploy`.
The `cross-compile` task calls the script `cross-compile.sh`[4].
The `deploy` tasks calls the script `deploy.sh`[5].

#### `cross-compile` task

As the name says here we try to cross-compile the project.

#### `deploy` task

The `deploy` task copies via scp the executable to the target, starts a `gdb server`
and connects via `remote gdb` from the host to the target.

### IntelliSense

`c_cpp_properties.json`[6] takes more or less care about VSC IntelliSense. Unfortunately it looks like `settings.json`[1] can not be used with `c_cpp_properties.json`[6].

[6] https://gitlab.com/exempli-gratia/mqtt-key-event-daemon/-/blob/master/.vscode/c_cpp_properties.json

# Bitbake

In `release mode` one would want to build this app.
This is currertly handled by `Bitbake`.

`mqtt-key-event-daemon_git.bb`[7] is the Bitbake recipe, which builds this application.

[7] https://gitlab.com/meta-layers/meta-u-boot-wic-bsp-beagle-bone-black-conserver/-/blob/master/recipes-apps/mqtt-key-event-daemon/mqtt-key-event-daemon_git.bb
