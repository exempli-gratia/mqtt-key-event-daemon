= install SDK ==

```
rber@t460s-2:/tmp$ ./resy-systemd-glibc-x86_64-core-image-minimal-base-conserver-devel-armv7at2hf-neon-beagle-bone-black-conserver-toolchain-4.1.66.sh 
Resy systemd (Reliable Embedded Systems Reference Distro) SDK installer version 4.1.66
======================================================================================
Enter target directory for SDK (default: /opt/resy-systemd/4.1.66): /home/rber/resy-systemd/4.1.66/multi-v7-ml/beagle-bone-black-conserver
You are about to install the SDK to "/home/rber/resy-systemd/4.1.66/multi-v7-ml/beagle-bone-black-conserver". Proceed [Y/n]? Y
Extracting SDK........................................................................................................................done
Setting it up...done
Icecc not found. Disabling distributed compiling
SDK has been successfully set up and is ready to be used.
Each time you wish to use the SDK in a new shell session, you need to source the environment setup script e.g.
 $ . /home/rber/resy-systemd/4.1.66/multi-v7-ml/beagle-bone-black-conserver/environment-setup-armv7at2hf-neon-resy-linux-gnueabi
```