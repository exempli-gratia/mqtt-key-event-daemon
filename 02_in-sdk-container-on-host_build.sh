HERE=$(pwd)
source ../../env.sh
source ../../../../common-scripts/common.sh
#from_host

#plus_off
#echo "+ source ${HOST_NFS}/ext-sdk/${YOCTO_VERSION}/${YOCTO_ML}/${TARGET_BOARD}/environment-setup-${YOCTO_TOOLCHAIN_SHORT}-${DISTRO}-linux-gnueabi"
#source ${HOST_NFS}/ext-sdk/${YOCTO_VERSION}/${YOCTO_ML}/${TARGET_BOARD}/environment-setup-${YOCTO_TOOLCHAIN_SHORT}-${DISTRO}-linux-gnueabi
#plus_on

plus_on

which cmake

if [ -d build ]; then
   rm -rf build
fi

set -x
mkdir build
pushd build
#cmake --debug-find ../src
cmake ../src
popd

pushd build
make clean
make
popd

set +x

cd ${HERE}
plus_echo "$(basename ${0}) done"
