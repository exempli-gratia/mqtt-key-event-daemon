#!/bin/bash
# this should not be called from the shell,
# but is called from VCS tasks
# e.g. 
#      build: CTRL + B
#      debug: F5

SDK_ENV="$1"

. ${SDK_ENV}

if [ -d build ]; then
   echo "@@@ removing build dir @@@"
   rm -rf build
fi

mkdir build
cd build
cmake ../src
cd ..

cd build
make clean; make -j$(nproc)
cd ..
