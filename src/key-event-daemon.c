/*
 * stdin publisher
 * 
 * compulsory parameters:
 * 
 * --topic topic to publish on
 * 
 * defaulted parameters:
 * 
 *  --host :: localhost
 *  --port :: 1883
 *  --qos :: 0
 *  --clientid :: stdin-publisher-async
 *  --maxdatalen :: 100
 *  --keepalive :: 10
 * 
 *  --userid :: none
 *  --password :: none
 *  
 * ./key-event-daemon /dev/input/by-path/platform-gpio-keys-event --host consrv-1
 */

#define KEY_EVENT_DAEMON_PRIVATE_PROTOTYPES
#define BUTTON_CONSTANTS_PRIVATE_PROTOTYPES
#include "global-include.h"

/* Global Status Variables for Different Application Stages */
MQTTAsync client;
/* for do {} while -- How many attempts to succeed */
static volatile atomic_int counter = 0;

static volatile atomic_int is_delivered = 0;
static volatile atomic_int connected = 0;	/* that's a shared resource */
static volatile atomic_int disconnected = 0;	/* that's a shared resource */

void getopts(int argc, char **argv);
void myconnect(MQTTAsync * client);

/* Flag that tells e.g. the daemon to exit. */
static volatile atomic_int exit_request = 0;

volatile int toStop = 0;	/* why is this volatile and not static ? */

/*$off*/
/*********************************************************************************************************************** */
/*!
 *  \brief    Signal handler
 *            SIGHUP:  do nothing - loop
 *            SIGTERM: sets exit_request flag -> exit
 *            SIGINT:  exit 
 *
 *  \return   void
 *
 *  \b ERRNO: 
 *
 *  \remarks  
 */
/*********************************************************************************************************************** */
/*$on*/
static void sig_handler(int sig /* signal */ )
{
	//syslog(LOG_DEBUG, "\n%s %3d: cfinish: signo=%s\n", __FILE__, __LINE__,strsignal(sig));
	zlog_debug(zc, "signo=%s", strsignal(sig));
	/* Find out which signal we're handling */
	switch (sig) {
	case SIGHUP:
		zlog_debug(zc, "Caught SIGHUB, do nothing -  we should loop?");
		break;
	case SIGTERM:
		/* we just set a flag - for afterwards */
		zlog_debug(zc, "Caught SIGTERM, set exit_request flag");
		exit_request = 1;
		break;
	case SIGINT:
		zlog_debug(zc, "Caught SIGINT, exiting now");
		exit(0);
	default:
		zlog_error(zc, "Caught wrong signal: %d", sig);
		return;
	}
}

/* 
 * Callback function. Implement the application logic in case 
 * of Asynchronous Connection-Filure
 */
void onConnectFailure(void *context, MQTTAsync_failureData * response)
{
	//printf("Connect failed, rc is %d\n", response ? response->code : -1); /* don't use ? - unreadable code */
	connected = -1;		/* not necessarily atomic */
}

/* 
 * This is a callback function. Enable asynchronous notification in 
 * case of successful `connection` completion.
 */
void onConnect(void *context, MQTTAsync_successData * response)
{
	//printf("Connected");

	/* Proceed to next stage ... Have a Connection to Server */
	connected = 1;		/* not necessarily atomic */
}

void connectionLost(void *context, char *cause)
{
	MQTTAsync client = (MQTTAsync) context;
	MQTTAsync_connectOptions conn_opts =
	    MQTTAsync_connectOptions_initializer;
	MQTTAsync_SSLOptions ssl_opts = MQTTAsync_SSLOptions_initializer;
	int rc = 0;
	ssl_opts = ssl_opts;

	cause = cause;

	zlog_debug(zc, "Connecting");
	conn_opts.keepAliveInterval = 10;
	conn_opts.cleansession = 1;
	conn_opts.username = opts.username;
	conn_opts.password = opts.password;
	conn_opts.onSuccess = onConnect;
	conn_opts.onFailure = onConnectFailure;
	conn_opts.context = client;

	ssl_opts.enableServerCertAuth = 0;
	/*conn_opts.ssl_opts = &ssl_opts; */

	connected = 0;

	if ((rc = MQTTAsync_connect(client, &conn_opts)) != MQTTASYNC_SUCCESS) {
		//syslog(LOG_ERR, "Failed to start connect, return code is %d\n",rc);
		zlog_error(zc, "Failed to start connect, return code is %d",
			   rc);
		exit(EXIT_FAILURE);
	}
}

/* 
 * Establish MQTT Connection of Client with Server/ Broker ...
 * On Top of TCP/ IP stack
 * Client sends a CONNECT message and Server responds with a CONNACK 
 */
void myconnect(MQTTAsync * client)
{
	/* 
	 * struct_id, struct_version, keepAliveInterval, cleansession,
	 * maxInFlight, username, password, connectTimeout, retryInterval,
	 * ssl, onSuccess, onFailure, context
	 */
	MQTTAsync_connectOptions conn_opts =
	    MQTTAsync_connectOptions_initializer;

	/* 
	 * struct_id, struct_version, trustStore, keyStore, privateKey,
	 * privateKeyPassword, enableCipherSuites, enableServerCertAuth
	 */
	MQTTAsync_SSLOptions ssl_opts = MQTTAsync_SSLOptions_initializer;

	int rc;

	/* 
	 * Configure Connection Parameters 
	 */
	conn_opts.keepAliveInterval = opts.keepalive;
	conn_opts.cleansession = 1;
	conn_opts.username = opts.username;
	conn_opts.password = opts.password;

	/* Pointer to callback function if connect SUCCESSFULLY completes */
	conn_opts.onSuccess = onConnect;
	/* Pointer to callback function if connect FAILS */
	conn_opts.onFailure = onConnectFailure;

	/* the context to be passed to callbacks */
	conn_opts.context = client;

	/* No SSL/ TLS connection using the OpenSSL library */
	ssl_opts.enableServerCertAuth = 0;

	conn_opts.automaticReconnect = 1;
	/*** Finished Connection Options Configuration ***/

	zlog_debug(zc, "Ready to Connect to Server");

	/*** How Many Attempts to Establish Client-Server Connection ***/
	/* Static/ Global Variable 
	 * Initialize here ... before connect attempts 
	 * Define Later ... onSuccess() callback = 1
	 */
	connected = 0;

	/* The safe case - Avoid It
	   if ( (rc = MQTTAsync_connect(*client, &conn_opts)) 
	   != MQTTASYNC_SUCCESS ) 
	   {
	   printf("Failed to connect, return code %d\n", rc);
	   exit(EXIT_FAILURE);
	   } */

	counter = 0;
	do {
		/* New Attempt */
		counter++;

		if ((rc = MQTTAsync_connect(*client, &conn_opts))
		    == MQTTASYNC_SUCCESS) {
			/* MQTT Server - Client Connection Established */
			break;
		} else {
			/* Log Connection Error Code */
			zlog_error(zc,
				   "MQTT Connection Failure, error code = %d",
				   rc);
		}
	} while (1);

	/* Outside execution loop - check how many attempts */
	if (counter > 1) {
		zlog_error(zc, "Many Attempts to Connect: %d", counter);
	} else {
		zlog_debug(zc, "Connected Immediately");
	}
}

int messageArrived(void *context, char *topicName, int topicLen,
		   MQTTAsync_message * m)
{
	context = context;
	topicName = topicName;
	topicLen = topicLen;
	m = m;
	/* Not expecting any messages */
	return 1;
}

/* 
 * This is a callback function. The client application should provide
 * an implementation to enable asynchronous nitification of message 
 * delivery to MQTT Server/ Broker.
 */
void deliveryComplete(void *context, MQTTAsync_token token)
{
	zlog_debug(zc, "Successful MQTT Message Delivery");
	is_delivered = 1;
}

void onDisconnect(void *context, MQTTAsync_successData * response)
{
	context = context;
	response = response;
	disconnected = 1;
}

static int published = 0;

void onPublish(void *context, MQTTAsync_successData * response)
{
	context = context;
	response = response;
	published = 1;
}

void onPublishFailure(void *context, MQTTAsync_failureData * response)
{
	context = context;
	response = response;
	zlog_error(zc, "Publish failed, return code is %d",
		   response ? response->code : -1);
	published = -1;
}

/* 
 * This function creates an MQTT client ready for connection to 
 * specified broker/ server. 
 * 
 */
void tryToCreateClient(char *url)
{
	/* Response Code */
	int rc;

	/* The necessary parameters to create async client ... Initialize & Customize */
	MQTTAsync_createOptions create_opts =
	    MQTTAsync_createOptions_initializer;
	create_opts.sendWhileDisconnected = 1;

	/* Persistence Types :
	 * MQTTCLIENT_PERSISTENCE_DEFAULT ... File system-based 
	 * MQTTCLIENT_PERSITSENCE_NONE ... In case of failure in-flight messages are lost 
	 **/
	rc = 0;
	rc = MQTTAsync_createWithOptions(&client, url, opts.clientid,
					 /* Use in-memory persistence */
					 MQTTCLIENT_PERSISTENCE_NONE,
					 NULL, &create_opts);
	if (rc != MQTTASYNC_SUCCESS) {
		// MQTT Client Failure
		// perror("mqttasync_createwithoptions()");

		zlog_error(zc, "Failed to start CREATE, return code `%d`", rc);

		/* Cannot creata an MQTT-Client to Broker ... Abord */
		exit(EXIT_FAILURE);
	}

	/* what should be happening with rc? 
	 * Error handling?
	 * since we are no in the loop here, maybe loop and retry? 
	 * (performance) Variable 'rc' is reassigned a value before the old one has been used. 
	 */

	/* I guess here you want to connect SIGINT 
	 * and SIGTERM with the sig_handler signal handler
	 */

	signal(SIGINT, sig_handler);
	signal(SIGTERM, sig_handler);

	/* After Successful Client Creation ... Assign Glocal Callback Functions */
	rc = 0;
	rc = MQTTAsync_setCallbacks(client,	/* client handle */
				    client,	/* Application-specific context is passed to callbacks */
				    connectionLost,	/* Pointer to MQTTAsync_connectionLost() to handle disconnections */
				    messageArrived,	/* Pointer to MQTTAsync_messageArrived() to handle reeipt of messages */
				    deliveryComplete);	/* Pointer to MQTTAsync_deliveryComplete() check for successful delivery */
	if (rc != MQTTASYNC_SUCCESS) {
		// perror("MQTTAsync_setCallbacks()");

		//syslog(LOG_ERR, "Failed to Set Callbacks, return code `%d`\n",rc);
		zlog_error(zc, "Failed to Set Callbacks, return code `%d`", rc);

		/* Cannot Assign Callbacks to Client ... Abort */
		exit(EXIT_FAILURE);
	}
}

void tryToDisconnect()
{
	int rc = 0;

	MQTTAsync_disconnectOptions disc_opts =
	    MQTTAsync_disconnectOptions_initializer;

	disc_opts.onSuccess = onDisconnect;

	if ((rc =
	     MQTTAsync_disconnect(client, &disc_opts)) != MQTTASYNC_SUCCESS) {
		//syslog(LOG_ERR, "Failed to start disconnect, return code is %d\n", rc);
		zlog_error(zc, "Failed to start disconnect, return code is %d",
			   rc);
		exit(EXIT_FAILURE);
	}

	while (!disconnected) {
		/* usleep(10000L); *//* POSIX.1-2008 removes the specification of usleep(). */
		sleep(1);
	}

	MQTTAsync_destroy(&client);
}

void tryToSend(const char *topic, int data_len, void *buffer)
{
	int rc = 0;

	MQTTAsync_responseOptions pub_opts =
	    MQTTAsync_responseOptions_initializer;

	pub_opts.onSuccess = onPublish;
	pub_opts.onFailure = onPublishFailure;

	do {
		rc = MQTTAsync_send(client, topic, data_len, buffer,
				    opts.qos, opts.retained, &pub_opts);
	} while (rc != MQTTASYNC_SUCCESS);
}

void unload_driver()
{
	int dynamic_cmd_size = 0;
	char *dynamic_cmd = NULL;
	int retval;

	/* driver */
	char driver_1[] = "gpio_keys_polled";
	/* check if driver is loaded */
	char cmd_1[] = "/bin/lsmod | /bin/grep ";
	/* unload driver */
	char cmd_2[] = "/sbin/modprobe -r ";
	/* load driver */
	//char cmd_3[] = "/sbin/modprobe ";
	/* --> check if driver is loaded */
	errno = 0;
	/* module already loaded:
	   retval = 0
	   module not loaded:
	   retval = 256
	 */
	dynamic_cmd_size = strlen(cmd_1) + strlen(driver_1) + 2;
	dynamic_cmd = malloc(sizeof(char) * dynamic_cmd_size);
	if (dynamic_cmd == NULL) {
		zlog_error(zc, "cmd_1: could not allocate memory");
		exit(EXIT_FAILURE);
	}
	snprintf(dynamic_cmd, dynamic_cmd_size, "%s%s\n", cmd_1, driver_1);
	zlog_info(zc, "dynamic_cmd:%s\n", dynamic_cmd);
	retval = system(dynamic_cmd);
	if (errno == -1) {
		zlog_error(zc, "%s failed: %s", dynamic_cmd, strerror(errno));

	}
	free(dynamic_cmd);
	/* <-- check if driver is loaded */
	/* --> only if driver alredy loaded - unload it */
	if (retval == 0) {
		errno = 0;

		dynamic_cmd_size = strlen(cmd_2) + strlen(driver_1) + 2;
		dynamic_cmd = malloc(sizeof(char) * dynamic_cmd_size);
		if (dynamic_cmd == NULL) {
			zlog_error(zc, "cmd_2: could not allocate memory");
			exit(EXIT_FAILURE);
		}
		snprintf(dynamic_cmd, dynamic_cmd_size, "%s%s\n", cmd_2,
			 driver_1);
		zlog_info(zc, "dynamic_cmd:%s\n", dynamic_cmd);
		retval = system(dynamic_cmd);
		if (errno == -1) {
			zlog_error(zc, "%s failed: %s", dynamic_cmd,
				   strerror(errno));

		}
		free(dynamic_cmd);
	}
	/* <-- only if driver alredy loaded - unload it */
}

void load_driver()
{
	int dynamic_cmd_size = 0;
	char *dynamic_cmd = NULL;
	int retval;

	/* driver */
	char driver_1[] = "gpio_keys_polled";
	/* check if driver is loaded */
	char cmd_1[] = "/bin/lsmod | /bin/grep ";
	/* unload driver */
	// char cmd_2[] = "/sbin/modprobe -r ";
	/* load driver */
	char cmd_3[] = "/sbin/modprobe ";

	/* --> load driver */
	errno = 0;

	dynamic_cmd_size = strlen(cmd_3) + strlen(driver_1) + 2;
	dynamic_cmd = malloc(sizeof(char) * dynamic_cmd_size);
	if (dynamic_cmd == NULL) {
		zlog_error(zc, "cmd_3: could not allocate memory");
		exit(EXIT_FAILURE);
	}
	snprintf(dynamic_cmd, dynamic_cmd_size, "%s%s\n", cmd_3, driver_1);
	zlog_info(zc, "dynamic_cmd:%s\n", dynamic_cmd);
	retval = system(dynamic_cmd);
	if (errno == -1) {
		zlog_error(zc, "%s failed: %s", dynamic_cmd, strerror(errno));

	}
	free(dynamic_cmd);
	/* <-- load driver */
}

/*
 * Main() 
 */
int main(int argc, char **argv)
{
	char url[100];
	char *topic = NULL;
	char *dynamic_topic = NULL;
	int dynamic_topic_size = 0;
	/* char topic[300]; */
	int rc = 0;
	char *buffer = NULL;

	char mqtt_msq[1000];
	char msg_1[300];
	char msg_2[300];
	char msg_3[300];
	char msg_4[300];

	int delim_len;

	/* The number of bytes read */
	ssize_t rd;

	/* identifier for keystrokes info */
	struct input_event ev[64];

	int i;
	int retval;

	/* select() system call configuration arguments 
	 * ******************************************** */
	struct timeval *pto;
	//struct timeval pto;

	/* select() ... returns number of ready file descriptors
	 *  0    on timeout
	 * -1   on failure
	 */
	int ready;

	/* File descriptor sets
	 * readfsd      .. set of file descriptors / if INPUT is possible 
	 * writefds     .. set of file descriptors / if OUTPUT is possible
	 * exceptfds    .. set of file descriptors / if EXCEPTIONAL condition occurred
	 */
	fd_set readfds;

	/* nfds .. should be set to the highest-number file descriptor 
	 *         in any of the three-sets plus(+) 1
	 */
	int nfds;

	/* An abstract indicator used to access an input/output resource */
	int fd;
	/* --> zlog */
	rc = zlog_init("/etc/key-event-daemon_zlog.conf");
	if (rc) {
		printf("init failed\n");
		return 2;
	}

	zc = zlog_get_category("key-event-daemon");
	if (!zc) {
		printf("get cat failed\n");
		zlog_fini();
		return 3;
	}
	/* <-- zlog */

	zlog_info(zc, "hello, zlog -- info");
	zlog_error(zc, "hello, zlog -- error");

	/* parse command line arguments */
	my_getopts(argc, argv);

	/* unload driver if it was already loaded */
	unload_driver();

	/* above me make sure that there is no other driver blocking access to the gpios */
	/* here we try to find out how the channels are set */
	gpiod_input();

	/* load driver */
	load_driver();

	/* Local Broker URL */
	/* int sprintf(char *buffer, const char *format-string, argument-list); */
	// sprintf(url, "%s:%s", opts.host, opts.port);
	/* int snprintf(char *buffer, size_t n, const char *format-string, argument-list); */
	/* The snprintf() function is identical to the sprintf() function with the addition
	   of the n argument, which indicates the maximum number of characters
	   (including the ending null character) to be written to buffer. */
	snprintf(url,
		 sizeof(opts.host) + sizeof(opts.port) + 2,
		 opts.host, opts.port);

	if (opts.verbose) {
		//printf("Broker URL is %s\n", url);
		//syslog(LOG_DEBUG, "MQTT Broker URL : `%s`\n", url);
		zlog_debug(zc, "MQTT Broker URL: `%s`", url);
	}

	topic = strdup(opts.topic);

	//syslog(LOG_DEBUG, "Using Topic %s [%d]\n", topic, strlen(topic));
	zlog_debug(zc, "Using Topic %s [%d]", topic, strlen(topic));

	/* *********************************************
	 * **** Device Driver File /dev/input/event ****
	 * ***  Start Monitoring with Blocking Read ****
	 * *********************************************/

	/* after connect MQTT is active?
	 * I guess like this the malloc should be before connect 
	 */

	/* Robert : where does opts.maxdatalen come from?
	 * commandline ? 
	 */

	/* Miltos : variable `maxdatalen` defines the size of buffer 
	 * used for storing the new Sensor/ Button data.
	 * The default value is 100
	 * A user-dedined value can be entered with command line --maxdatalen   
	 */
	buffer = malloc(opts.maxdatalen);
	if (buffer == NULL) {
		zlog_error(zc, "opts.maxdatalen: could not allocate memory");
		exit(EXIT_FAILURE);
	}

	/* ****************************************** */
	/* **** SELECT() & PSELECT() system call **** */
	/* ****************************************** */

	/* Timeout for select() */
	pto = NULL;		/* Infinite timeout */
	//pto->tv_sec = 5;
	//pto->tv_usec = 0;

	//pto.tv_sec = 10;
	//pto.tv_usec = 50000;

	/* Initialize FDS */
	FD_ZERO(&readfds);

	/* Open Device File */
	/* we can actually for fun use non blocking read, 
	   since select should have awoken us here 
	 */
	errno = 0;
	if ((fd = open(DEVICE_PATH, O_RDONLY)) < 0) {
		/* failed */
		zlog_error(zc, "open driver failed: %s", strerror(errno));
		exit(EXIT_FAILURE);
	} else {
		/* success */
		zlog_debug(zc, "open driver success: %s", strerror(errno));
	}

	/* Let SELECT() monitor if input is possible in Sensor/ Button Driver */
	FD_SET(fd, &readfds);

	/* Highest numbered file descriptor */
	nfds = 0;
	if (fd > nfds) {
		nfds = fd + 1;
	}

	/* Is there a specific reason those are not global variables ? */
	/* as global vars we could move MQTT malas in specific functions */

	/* *************************************
	 * ************ MQTT Code **************
	 * *** Create, Configure, Open, Send ***
	 * *************************************/

	/* First Create the MQTT-Client ... Ready to be Connected */
	tryToCreateClient(url);

	/* also this could go into some different function out of main */

	/* what should be happening with rc? 
	 * Error handling?
	 * since we are no in the loop here, maybe loop and retry? 
	 * (performance) Variable 'rc' is reassigned a value before the old one has been used. 
	 */

	/* After successful MQTT-Client Create ... 
	 * **** Try to Esatblish Connection to MQTT-Server/ Broker ****
	 */
	//syslog(LOG_DEBUG, "Before myconnect\n");
	myconnect(&client);
	//syslog(LOG_DEBUG, "After myconnect\n");

	/* send initial values before entering event loop 
	 * 
	 * topic: 
	 * /dev/input/by-path/platform-gpio-keys-event
	 *
	 * channel 0: 
	 * /dev/input/by-path/platform-gpio-keys-event/type/1/code/100/value/1 or 0
	 *                                                         ^^^       ^
	 */

	/* printf("!!!!! topic: %s\n", topic); */
	/* dynamic_topic_size = strlen(topic)+strlen(value); */
	//printf("all: %s\n",val_to_string_func(topic, 1));
	for (int i = 0; i < LINES; i++) {
		tryToSendChannel(topic, i);
	}

	/* Evtest Code Snippet .. */
	while (1) {
#if 1
		zlog_debug(zc, "before select: %s", strerror(errno));
#endif
		ready = select(nfds, &readfds, NULL, NULL, pto);
		//ready = select(nfds, &readfds, NULL, NULL, &pto);
#if 1
		zlog_debug(zc, "after select: %s", strerror(errno));
#endif
	/** New Sensor/ Button Data Arrived **/
#if 1
		zlog_debug(zc, "before read: %s", strerror(errno));
#endif
		rd = read(fd, ev, sizeof(struct input_event) * 64);
#if 1
		zlog_debug(zc, "after read: %s", strerror(errno));
#endif
//        close(fd);
//        goto before_select;

		for (i = 0; i < rd / sizeof(struct input_event); i++) {
				struct timeval tval;
				if (gettimeofday(&tval, NULL)) {
                   perror("gettimeofday failed");
                   return 7;
                }
			if (ev[i].type == EV_SYN) {
#if 0
				sprintf(msg_1,
					"Event: time %ld.%06ld, -------- %s --------\n",
					ev[i].time.tv_sec, ev[i].time.tv_usec,
					ev[i].code ? "Config Sync" :
					"Report Sync");
#else
				sprintf(msg_1,
					"Event: time %lld.%06lld, -------- %s --------\n",
					tval.tv_sec, tval.tv_usec,
					ev[i].code ? "Config Sync" :
					"Report Sync");
#endif
			} else if (ev[i].type == EV_MSC
				   && (ev[i].code == MSC_RAW
				       || ev[i].code == MSC_SCAN)) {
#if 0
				sprintf(msg_2,
					"Event: time %ld.%06ld, type %d (%s), code %d (%s), value %02x\n",
					ev[i].time.tv_sec, ev[i].time.tv_usec,
					ev[i].type,
					events[ev[i].
					       type] ? events[ev[i].type] : "?",
					ev[i].code, names[ev[i].type]
					? (names[ev[i].type][ev[i].code] ?
					   names[ev[i].
						 type][ev[i].code] : "?") : "?",
					ev[i].value);
#else
				sprintf(msg_2,
					"Event: time %lld.%06lld, type %d (%s), code %d (%s), value %02x\n",
					tval.tv_sec, tval.tv_usec,
					ev[i].type,
					events[ev[i].
					       type] ? events[ev[i].type] : "?",
					ev[i].code, names[ev[i].type]
					? (names[ev[i].type][ev[i].code] ?
					   names[ev[i].
						 type][ev[i].code] : "?") : "?",
					ev[i].value);
#endif
			} else {
				/* @@@ TODO: sprintf -> snprintf */
				sprintf(msg_3,
					"/type/%d/code/%d/value/%d",
					ev[i].type, ev[i].code, ev[i].value);
				sprintf(msg_4, "%d", ev[i].value);
			}
		}

		zlog_debug(zc, "msg_3: %s", msg_3);
		zlog_debug(zc, "msg_4: %s", msg_4);

		dynamic_topic_size = strlen(topic) + strlen(msg_3);
#if 0
		zlog_debug(zc, "dynamic_topic_size: %d", dynamic_topic_size);
#endif
		errno = 0;
		dynamic_topic = malloc(sizeof(char) * dynamic_topic_size);
		if (dynamic_topic == NULL) {
			zlog_error(zc,
				   "dynamic_topic: could not allocate memory");
			exit(EXIT_FAILURE);
		}
		snprintf(dynamic_topic, dynamic_topic_size, "%s%s\n", topic,
			 msg_3);
		zlog_debug(zc, "Composed topic: %s", dynamic_topic);
		zlog_debug(zc, "Message: %s", msg_4);

		/* tryToSend(topic, strlen(msg_3), msg_3); */
		tryToSend(dynamic_topic, strlen(msg_4), msg_4);
		free(dynamic_topic);
	}

	while (!toStop) {
		int data_len = 0;

		/* 
		 * I/O Multiplexing .. 
		 * Simultaneous monitor many file descriptors and see if 
		 * read/ write is possible on any of them. 
		 */

		/* ************ End of select() system call **********
		 * ******* to monitor multiple file descriptors ****** 
		 */

		/* here should be pselect instead of select */
		/* something like:
		   sigaddset(&ss, SIGWHATEVER);
		   ready = pselect(nfds, &readfds, NULL, NULL, pto, &ss);
		 */
		//ready = select(nfds, &readfds, NULL, NULL, pto);

		if (ready == -1) {
			/* An error occured */
			perror("select");
			exit(EXIT_FAILURE);
		}
		/* else */
		if (ready == 0) {
			/* Call was timed out */
			;
		}

	}

	zlog_debug(zc, "Stopping");

	free(buffer);

	tryToDisconnect();

	return EXIT_SUCCESS;
}

/*
 * End of Main()
 */
