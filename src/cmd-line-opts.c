#include "global-include.h"
#include <getopt.h>
extern struct options opts;

/*
longopts is a pointer to the first element of an array of struct
       option declared in <getopt.h> as

           struct option {
               const char *name;
               int         has_arg;
               int        *flag;
               int         val;
           };

       The meanings of the different fields are:

       name   is the name of the long option.

       has_arg
              is: no_argument (or 0) if the option does not take an
              argument; required_argument (or 1) if the option requires
              an argument; or optional_argument (or 2) if the option
              takes an optional argument.

       flag   specifies how results are returned for a long option.  If
              flag is NULL, then getopt_long() returns val.  (For
              example, the calling program may set val to the equivalent
              short option character.)  Otherwise, getopt_long() returns
              0, and flag points to a variable which is set to val if
              the option is found, but left unchanged if the option is
              not found.

       val    is the value to return, or to load into the variable
              pointed to by flag.

       The last element of the array has to be filled with zeros.
*/
static const struct option longopts[] = {
        {"host",       required_argument, 0, 'H'},
        {"port",       required_argument, 0, 'P'},
        {"qos",        required_argument, 0, 'q'},
		{"retained",   required_argument, 0, 'r'},
		{"clientid",   required_argument, 0, 'c'},
		{"maxdatalen", required_argument, 0, 'm'},
		{"username",   required_argument, 0, 'u'},
		{"password",   required_argument, 0, 'p'},
		{"keepalive",  required_argument, 0, 'k'},
		{"help",       no_argument,       0, 'h'},
		{"daemonize",  required_argument, 0, 'd'},
		{"verbose",    no_argument,       0, 'v'},
		{"topic",      required_argument, 0, 't'},
        {0, 0, 0, 0},
};

void usage_success(void)
{
	usage();
}

void usage_failure(void)
{
	usage();
	exit(EXIT_FAILURE);
}

void usage(void)
{
	printf("MQTT stdin publisher\n");
	printf("Usage: stdinpub topicname <options>, where options are:\n");
	printf("    --host <hostname> (default is %s)\n", opts.host);
	printf("    --port <port> (default is %s)\n", opts.port);
	printf("    --qos <qos> (default is %d)\n", opts.qos);
	printf("          valid values: 0 or 1 or 2\n");
	printf("    --retained (default is %d)\n", opts.retained);
	printf("          valid values: 0 (off) or 1 (on)\n");
	printf("    --clientid <clientid> (defaults is %s)\n", opts.clientid);
	printf("    --maxdatalen <bytes> (default is %d)\n", opts.maxdatalen);
	printf("    --username <username> (default is %s)\n",opts.username);
	printf("          null means no username\n");
	printf("    --password <password> (default is %s)\n",opts.password);
	printf("          null means no password\n");
	printf("    --keepalive <seconds> (default is %d seconds)\n",opts.keepalive);
	printf("    --verbose <verbose> (default is %d)\n",opts.verbose);
	printf("          valid values: 0 (verbose logging off) or 1 (verbose logging on)\n");
	printf("    --daemonize <daemonize> (default is %d)\n",opts.daemonize);
	printf("          valid values: 0 (not daemonized) or 1 (daemonized)\n");
	printf("    --topic <topic> (default is %s)\n",opts.topic);
	hostname[1023] = '\0';
	gethostname(hostname, 1023);
	printf
	    ("\nkey-event-daemon /dev/input/by-path/platform-gpio-keys-event --host %s\n\n",
	     hostname);
	//exit(EXIT_FAILURE);
}

#if 0
void getopts(int argc, char **argv)
{
	int count = 2;

	while (count < argc) {
		if (strcmp(argv[count], "--retained") == 0) {
			opts.retained = 1;
		}

		if (strcmp(argv[count], "--verbose") == 0) {
			opts.verbose = 1;
		}

		else if (strcmp(argv[count], "--qos") == 0) {
			if (++count < argc) {
				if (strcmp(argv[count], "0") == 0) {
					opts.qos = 0;
				} else if (strcmp(argv[count], "1") == 0) {
					opts.qos = 1;
				} else if (strcmp(argv[count], "2") == 0) {
					opts.qos = 2;
				} else {
					usage();
				}
			} else {
				usage();
			}
		} else if (strcmp(argv[count], "--host") == 0) {
			if (++count < argc) {
				opts.host = argv[count];
			} else {
				usage();
			}
		} else if (strcmp(argv[count], "--port") == 0) {
			if (++count < argc) {
				opts.port = argv[count];
			} else {
				usage();
			}
		} else if (strcmp(argv[count], "--clientid") == 0) {
			if (++count < argc) {
				opts.clientid = argv[count];
			} else {
				usage();
			}
		} else if (strcmp(argv[count], "--username") == 0) {
			if (++count < argc) {
				opts.username = argv[count];
			} else {
				usage();
			}
		} else if (strcmp(argv[count], "--password") == 0) {
			if (++count < argc) {
				opts.password = argv[count];
			} else {
				usage();
			}
		} else if (strcmp(argv[count], "--maxdatalen") == 0) {
			if (++count < argc) {
				opts.maxdatalen = atoi(argv[count]);
			} else {
				usage();
			}
		} else if (strcmp(argv[count], "--delimiter") == 0) {
			if (++count < argc) {
				opts.delimiter = argv[count];
			} else {
				usage();
			}
		} else if (strcmp(argv[count], "--keepalive") == 0) {
			if (++count < argc) {
				opts.keepalive = atoi(argv[count]);
			} else {
				usage();
			}

		}

		count++;

	}
}
#else

/* @@@TODO:
   at least we need to pass the hostname:
   ./key-event-daemon --host $HOSTNAME
   this needs to be enforced here, which is not the case
*/

void my_getopts(int argc, char **argv)
{
	int opt;

    printf("argc: %d\n",argc);

	if ( argc == 1) {
		printf("no arguments passed - using defaults\n");
		usage_success();
	}

	while ((opt = getopt_long(argc, argv, "HPqrcmupktvdh:?", longopts, NULL)) != -1) {
                printf("looping\n");
                switch (opt) {
                case '?':
                case 'h':
						printf("    --help\n");
                        usage_failure();
                        break;
				case 'H':
						opts.host = strdup(optarg);
						printf("    --host <%s>\n", opts.host);
						break;
				case 'P':
						opts.port = strdup(optarg);
						printf("    --port <%s>\n", opts.port);
						break;
				case 'q':
				        int i = atoi(optarg);
						if (i>=0 && i<=2 ) {
							opts.qos = i;
							printf("    --qos <%d>\n", opts.qos);
						} else {
							printf("    --qos <%d> is out of range\n", i);
							usage_failure();
						}
						break;
				case 'r':
				        int j = atoi(optarg);
						if (j>=0 && j<=1 ) {
							opts.retained = j;
							printf("    --retained <%d> i.e. %s \n", opts.retained, opts.retained ? "on" : "off");
						} else {
							printf("    --retained <%d> is out of range\n", j);
							usage_failure();
						}
						break;
				case 'c':
						opts.clientid = strdup(optarg);
						printf("    --clientid <%s>\n", opts.clientid);
						break;
				case 'm':
				        opts.maxdatalen = atoi(optarg);
						printf("    --maxdatalen <%d>\n", opts.maxdatalen);
						//printf("!!! don't pass this it's buggy !!!\n");
						break;
				case 'u':
						opts.username = strdup(optarg);
						printf("    --username <%s>\n", opts.username);
						break;
				case 'p':
						opts.password = strdup(optarg);
						printf("    --password <%s>\n", opts.password);
						break;
				case 'k':
				        opts.keepalive = atoi(optarg);
						printf("    --keepalive <%d> seconds\n", opts.keepalive);
						break;
				case 'd':
				        int l = atoi(optarg);
						if (l>=0 && l<=1 ) {
							opts.daemonize = l;
							printf("    --daemonize <%d> i.e. %s \n", opts.daemonize, opts.daemonize ? "daemonized" : "not daemonized");
							/* change to the "/" directory */
							int nochdir = 0;

							/* redirect standard input, output and error to /dev/null
							   this is equivalent to "closing the file descriptors"
							*/
							int noclose = 0;

							/* glibc call to daemonize this process without a double fork */
							if(daemon(nochdir, noclose))
								perror("daemon");
						} else {
							printf("    --daemonize <%d> is out of range\n", l);
							usage_failure();
						}
						break;
				case 't':
						opts.topic = strdup(optarg);
						printf("    --topic <%s>\n", opts.topic);
						break;
				case 'v':
				        int k = atoi(optarg);
						if (k>=0 && k<=1 ) {
							opts.verbose = k;
							printf("    --verbose <%d> i.e. %s \n", opts.verbose, opts.verbose ? "on" : "off");
						} else {
							printf("    --verbose <%d> is out of range\n", k);
							usage_failure();
						}
						break;
                default:
                        usage_failure();
                        break;
                }
        }
        if (argv[optind + 1] == NULL) {
                usage_failure();
		}
}
#endif
