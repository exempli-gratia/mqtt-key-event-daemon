#ifndef __GLOBAL_INCLUDE_H__
#define __GLOBAL_INCLUDE_H__ 1

/*$3 ===================================================================================================================
    $C                                             Included headers
 ======================================================================================================================= */

/* Included headers before this point */
#ifdef __cplusplus
extern "C" {
#endif				/* __cplusplus */

/*$3 ===================================================================================================================
    $C                                                  Macros
 ======================================================================================================================= */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/select.h>

#include <stdatomic.h>

/* struct input_event */
#include <linux/input.h>

/* Logmask for Informational, Debug and Error Messages */
#include <syslog.h>

#include <sys/socket.h>
#include <netdb.h>

#include <MQTTAsync.h>
#include <zlog.h>

#include "button_constants.h"

#define DEVICE_PATH "/dev/input/event0"
#define LOG_LEVEL LOG_DEBUG

/* Note: delimiter is always \n at the moment */

	struct options {
		char *clientid;
		int maxdatalen;
		int qos;
		int retained;
		char *username;
		char *password;
		char *host;
		char *port;
		int verbose;
		int keepalive;
		char* topic;
		int daemonize;
	};

/* differentiate between public and private prototypes */
#if defined(KEY_EVENT_DAEMON_PRIVATE_PROTOTYPES)
#define EXTERN
	struct options opts =
	    { "stdin-publisher-async", 100, 0, 0, NULL, NULL, "localhost",
		"1883", 1, 10, "/dev/input/by-path/platform-gpio-keys-event", 0,
	};
#else
#define EXTERN  extern
	EXTERN struct options opts;
#endif

#ifndef LINES
#define LINES 7
#endif

    EXTERN zlog_category_t *zc;
	EXTERN int usb_input_val[LINES];

	EXTERN void usage(void);
	EXTERN int gpiod_input(void);
	EXTERN char hostname[1024];
	EXTERN void tryToSend(const char *topic, int data_len, void *buffer);
	EXTERN void tryToSendChannel(char *topic, int channel);
	EXTERN void my_getopts(int argc, char **argv);
#undef EXTERN

#ifdef __cplusplus
}
#endif				/* __cplusplus */
#endif				/* __HALF_DUPLEX_H__ */
/* 
 * EOF 
 */
