// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2017-2021 Bartosz Golaszewski <bartekgola@gmail.com>
// SPDX-FileCopyrightText: 2022 Kent Gibson <warthog618@gmail.com>

#include "global-include.h"
#include <getopt.h>
#include <gpiod.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

#include "tools-common.h"

/* --> */
int myargc;
char **myargv;
/* <-- */

struct config {
	bool active_low;
	bool by_name;
	bool numeric;
	bool strict;
	bool unquoted;
	enum gpiod_line_bias bias;
	enum gpiod_line_direction direction;
	unsigned int hold_period_us;
	const char *chip_id;
	const char *consumer;
};

static void print_help(void)
{
	printf("Usage: %s [OPTIONS] <line>...\n", get_progname());
	printf("\n");
	printf("Read values of GPIO lines.\n");
	printf("\n");
	printf("Lines are specified by name, or optionally by offset if the chip option\n");
	printf("is provided.\n");
	printf("\n");
	printf("Options:\n");
	printf("  -a, --as-is\t\tleave the line direction unchanged, not forced to input\n");
	print_bias_help();
	printf("      --by-name\t\ttreat lines as names even if they would parse as an offset\n");
	printf("  -c, --chip <chip>\trestrict scope to a particular chip\n");
	printf("  -C, --consumer <name>\tconsumer name applied to requested lines (default is 'gpioget')\n");
	printf("  -h, --help\t\tdisplay this help and exit\n");
	printf("  -l, --active-low\ttreat the line as active low\n");
	printf("  -p, --hold-period <period>\n");
	printf("\t\t\twait between requesting the lines and reading the values\n");
	printf("      --numeric\t\tdisplay line values as '0' (inactive) or '1' (active)\n");
	printf("  -s, --strict\t\tabort if requested line names are not unique\n");
	printf("      --unquoted\tdon't quote line names\n");
	printf("  -v, --version\t\toutput version information and exit\n");
	print_chip_help();
	print_period_help();
}

static int parse_config(int myargc, char **myargv, struct config *cfg)
{
	static const struct option longopts[] = {
		{ "active-low",	no_argument,		NULL,	'l' },
		{ "as-is",	no_argument,		NULL,	'a' },
		{ "bias",	required_argument,	NULL,	'b' },
		{ "by-name",	no_argument,		NULL,	'B' },
		{ "chip",	required_argument,	NULL,	'c' },
		{ "consumer",	required_argument,	NULL,	'C' },
		{ "help",	no_argument,		NULL,	'h' },
		{ "hold-period", required_argument,	NULL,	'p' },
		{ "numeric",	no_argument,		NULL,	'N' },
		{ "strict",	no_argument,		NULL,	's' },
		{ "unquoted",	no_argument,		NULL,	'Q' },
		{ "version",	no_argument,		NULL,	'v' },
		{ GETOPT_NULL_LONGOPT },
	};

	static const char *const shortopts = "+ab:c:C:hlp:sv";

	int opti, optc;

	memset(cfg, 0, sizeof(*cfg));
	cfg->direction = GPIOD_LINE_DIRECTION_INPUT;
	cfg->consumer = "gpioget";

	for (;;) {
		optc = getopt_long(myargc, myargv, shortopts, longopts, &opti);
		if (optc < 0)
			break;

		switch (optc) {
		case 'a':
			cfg->direction = GPIOD_LINE_DIRECTION_AS_IS;
			break;
		case 'b':
			cfg->bias = parse_bias_or_die(optarg);
			break;
		case 'B':
			cfg->by_name = true;
			break;
		case 'c':
			cfg->chip_id = optarg;
			break;
		case 'C':
			cfg->consumer = optarg;
			break;
		case 'l':
			cfg->active_low = true;
			break;
		case 'N':
			cfg->numeric = true;
			break;
		case 'p':
			cfg->hold_period_us = parse_period_or_die(optarg);
			break;
		case 'Q':
			cfg->unquoted = true;
			break;
		case 's':
			cfg->strict = true;
			break;
		case 'h':
			print_help();
			exit(EXIT_SUCCESS);
		case 'v':
			print_version();
			exit(EXIT_SUCCESS);
		case '?':
			die("try %s --help", get_progname());
		case 0:
			break;
		default:
			abort();
		}
	}

	return optind;
}

int gpiod_input(void)
{
	struct gpiod_line_settings *settings;
	struct gpiod_request_config *req_cfg;
	struct gpiod_line_request *request;
	struct gpiod_line_config *line_cfg;
	struct line_resolver *resolver;
	enum gpiod_line_value *values;
	struct resolved_line *line;
	struct gpiod_chip *chip;
	unsigned int *offsets;
	int i, num_lines, ret;
	struct config cfg;
	const char *fmt;

/* --> simulate argc, argv */
	#define ARGC 10
	#define MAXARGVS 100

	myargc=ARGC;

    /* let's allocate MAXARGVS for all myargvs */
	myargv = malloc(sizeof(char *)*MAXARGVS);
	memset(myargv,0,sizeof(char *)*MAXARGVS);

    /* myargv[0] is myargv[0] - name of exe if argv */
	myargv[ARGC-9]="-a";
	myargv[ARGC-8]="--active-low";
	myargv[ARGC-7]="usbin0";
	myargv[ARGC-6]="usbin1";
	myargv[ARGC-5]="usbin2";
	myargv[ARGC-4]="usbin3";
	myargv[ARGC-3]="usbin4";
	myargv[ARGC-2]="usbin5";
	myargv[ARGC-1]="usbin6";
	myargv[ARGC]=NULL;
/* <-- simulate argc, argv */

/* --> */
    #define LINES 7
	static int not_val[LINES];
/* <-- */

	i = parse_config(myargc, myargv, &cfg);
	myargc -= i;
	myargv += i;

	if (myargc < 1)
		die("at least one GPIO line must be specified");

	resolver = resolve_lines(myargc, myargv, cfg.chip_id, cfg.strict,
				 cfg.by_name);
	validate_resolution(resolver, cfg.chip_id);

	offsets = calloc(resolver->num_lines, sizeof(*offsets));
	values = calloc(resolver->num_lines, sizeof(*values));
	if (!offsets || !values)
		die("out of memory");
	settings = gpiod_line_settings_new();
	if (!settings)
		die_perror("unable to allocate line settings");

	gpiod_line_settings_set_direction(settings, cfg.direction);

	if (cfg.bias)
		gpiod_line_settings_set_bias(settings, cfg.bias);

	if (cfg.active_low)
		gpiod_line_settings_set_active_low(settings, true);

	req_cfg = gpiod_request_config_new();
	if (!req_cfg)
		die_perror("unable to allocate the request config structure");

	line_cfg = gpiod_line_config_new();
	if (!line_cfg)
		die_perror("unable to allocate the line config structure");

	gpiod_request_config_set_consumer(req_cfg, cfg.consumer);

	for (i = 0; i < resolver->num_chips; i++) {
		chip = gpiod_chip_open(resolver->chips[i].path);
		if (!chip)
			die_perror("unable to open chip '%s'",
				   resolver->chips[i].path);

		num_lines = get_line_offsets_and_values(resolver, i, offsets,
							NULL);

		gpiod_line_config_reset(line_cfg);
		ret = gpiod_line_config_add_line_settings(line_cfg, offsets,
							  num_lines, settings);
		if (ret)
			die_perror("unable to add line settings");

		request = gpiod_chip_request_lines(chip, req_cfg, line_cfg);
		if (!request)
			die_perror("unable to request lines");

		if (cfg.hold_period_us)
			usleep(cfg.hold_period_us);

		ret = gpiod_line_request_get_values(request, values);
		if (ret)
			die_perror("unable to read GPIO line values");

		set_line_values(resolver, i, values);

		gpiod_line_request_release(request);
		gpiod_chip_close(chip);
	}

	fmt = cfg.unquoted ? "%s=%s" : "\"%s\"=%s";

    /* here all went well and we get the result */

	for (i = 0; i < resolver->num_lines; i++) {
		line = &resolver->lines[i];
		if (cfg.numeric)
			printf("%d", line->value);
		else {
			printf(fmt, line->id,
			       line->value ? "active" : "inactive");
			not_val[i]=line->value; /* not_val */
		}
		if (i != resolver->num_lines - 1)
			printf(" ");
	}
	printf("\n");

/* --> */

	printf("channel              0 1 2 3 4 5 6\n");
	printf("values: [0] .... [6] %d %d %d %d %d %d %d\n",
	       not_val[0], not_val[1], not_val[2], not_val[3],
	       not_val[4], not_val[5], not_val[6]);

/* <-- */
	free_line_resolver(resolver);
	gpiod_request_config_free(req_cfg);
	gpiod_line_config_free(line_cfg);
	gpiod_line_settings_free(settings);
	free(offsets);
	free(values);

	return EXIT_SUCCESS;
}

void tryToSendChannel(char *topic, int channel)
{
	int dynamic_topic_size = 0;
	char *dynamic_topic = NULL;
	char mqtt_msq[1000];
/* first is topic */
/* msg_1 = /type/1/code/ */
	char msg_1[] = "/type/1/code/";
	int msg_2_int = 100;
/* msg_2 = 100 (button number) */
	char msg_2[] = "100";
/* msg_3 = /value/ */
	char msg_3[] = "/value/";
/* msg_4 = 1 or 0 (value) */
	char msg_4[] = "123";

	/* send initial values before entering event loop 
	 * 
	 * topic: 
	 * /dev/input/by-path/platform-gpio-keys-event
	 *
	 * channel 0: 
	 * /dev/input/by-path/platform-gpio-keys-event/type/1/code/100/value/1 or 0
	 *                                                         ^^^       ^
	 */

	/* printf("%s %3d: topic: %s\n", __FILE__, __LINE__, topic); */

	msg_2_int = 100 + channel;

	snprintf(msg_4, strlen(msg_4), "%d\n", usb_input_val[channel]);

	dynamic_topic_size =
	    strlen(topic) + strlen(msg_1) + strlen(msg_2) + strlen(msg_3) + 1;
	dynamic_topic = malloc(sizeof(char) * dynamic_topic_size);
	if (dynamic_topic == NULL) {
		syslog(LOG_ERR, "Could not allocate memory\n");
	}
	snprintf(dynamic_topic, dynamic_topic_size, "%s%s%d%s\n", topic, msg_1,
		 msg_2_int, msg_3);
	/* printf("dynamic_topic: %s\n", dynamic_topic); */

	tryToSend(dynamic_topic, strlen(msg_4), msg_4);
	free(dynamic_topic);

}
